local fn = vim.fn

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

Lazy = require("lazy")

local plugins = {
    -- lua utilities
    "nvim-lua/plenary.nvim",
    "nvim-lua/popup.nvim",  -- required by (at least) telescope.nvim
    "rafcamlet/nvim-luapad",

    -- fennel config
    'rktjmp/hotpot.nvim',

    -- colors and appearance
    "ryanoasis/vim-devicons",
    "kyazdani42/nvim-web-devicons",

    -- languages and file types
    "JuliaEditorSupport/julia-vim",
    "rust-lang/rust.vim",
    "lervag/vimtex",
    "preservim/vim-markdown",
    "kaarmu/typst.vim",

    -- statusline
    "nvim-lualine/lualine.nvim",

    -- buffer bar
    "akinsho/bufferline.nvim",

    -- file explorer
    "mikavilpas/yazi.nvim",

    -- completion
    "hrsh7th/nvim-cmp",
    "hrsh7th/cmp-buffer",
    "hrsh7th/cmp-path",
    "hrsh7th/cmp-nvim-lua",
    "saadparwaiz1/cmp_luasnip",
    "https://gitlab.com/ExpandingMan/cmp-latex",
    "racer-rust/vim-racer",

    -- which key
    "folke/which-key.nvim",

    -- terminal
    "akinsho/toggleterm.nvim",
    "hkupty/iron.nvim",

    -- search
    "nvim-telescope/telescope.nvim",
    "windwp/nvim-spectre",
    "cshuaimin/ssr.nvim",

    -- motions
    "ggandor/leap.nvim",

    -- editing
    "kylechui/nvim-surround",

    -- snippets
    "L3MON4D3/LuaSnip",

    -- registers
    "tversteeg/registers.nvim",

    -- todo comments
    "folke/todo-comments.nvim",
    "folke/trouble.nvim",

    -- sessions
    "rmagatti/auto-session",

    -- improved commandline
    "romgrk/fzy-lua-native",
    "gelguy/wilder.nvim",

    -- treesitter
    "nvim-treesitter/nvim-treesitter",
    "stevearc/aerial.nvim",
    "CKolkey/ts-node-action",

    -- LSP
    "neovim/nvim-lspconfig",
    "glepnir/lspsaga.nvim",
}

Lazy.setup(plugins)

--initialize hotpot for loading fennel files
require("hotpot").setup()
