local Telescope = require("telescope")
local Sorters = require("telescope/sorters")
local Previewers = require("telescope/previewers")

-- TODO make sure dotfiles can install bat

Telescope.setup({
    defaults = {
        vimgrep_arguments = {
            "rg",
            "--color=never",
            "--no-heading",
            "--with-filename",
            "--line-number",
            "--column",
            "--smart-case",
        },
        prompt_prefix = "◗ ",
        selection_caret = "◗ ", 
        entry_prefix = " ",
        initial_mode = "insert",
        selection_strategy = "reset",
        sorting_strategy = "descending",
        layout_strategy = "horizontal",
        layout_config = {
            --horizontal = {width=0.8, mirror=false},
            --vertical = {width=0.8, mirror=false},
        },
        file_sorter = Sorters.get_fuzzy_file,
        file_ignore_patterns = {},
        generic_sorter = Sorters.get_generic_fuzzy_sorter,
        winblend = 0,
        results_hight = 42,
        border = {},
        borderchars = { '─', '│', '─', '│', '╭', '╮', '╯', '╰' },
        color_devicons = true,
        use_less = true,
        path_display = {},
        set_env = {['COLORTERM'] = 'truecolor'},
        file_previewer = Previewers.vim_buffer_cat.new,
        grep_previewer = Previewers.vim_buffer_vimgrep.new,
        qflist_previewer = Previewers.vim_buffer_qflist.new,
    }
})

WhichKey.add {
  { "\\", group = "search", remap = false },
  { "\\R", "<cmd>Telescope registers<CR>", desc = "search vim registers", remap = false },
  { "\\V", "<cmd>Telescope vim_options<cr>", desc = "search vim options", remap = false },
  { "\\\\", "<cmd>Telescope live_grep<CR>", desc = "live grep", remap = false },
  { "\\b", "<cmd>Telescope buffers<CR>", desc = "search buffers", remap = false },
  { "\\c", "<cmd>Telescope commands<CR>", desc = "search vim commands", remap = false },
  { "\\d", "<cmd>Telescope lsp_definitions<CR>", desc = "search LSP definitions", remap = false },
  { "\\f", "<cmd>Telescope find_files<CR>", desc = "find files", remap = false },
  { "\\h", "<cmd>Telescope help_tags<CR>", desc = "search help tags", remap = false },
  { "\\k", "<cmd>Telescope keymaps<CR>", desc = "search key mappings", remap = false },
  { "\\l", "<cmd>Telescope lsp_references<CR>", desc = "search LSP references", remap = false },
  { "\\m", "<cmd>Telescope marks<cr>", desc = "search marks", remap = false },
  { "\\o", "<cmd>Telescope grep_string<cr>", desc = "search for string under cursor", remap = false },
  { "\\q", "<cmd>Telescope current_buffer_fuzzy_find<CR>", desc = "search current buffer", remap = false },
  { "\\s", require("auto-session.session-lens").search_session, desc = "search sessions", remap = false },
  { "\\w", "<cmd>Telescope highlights<CR>", desc = "search nvim highlight groups", remap = false },
}

-- appearance
vim.api.nvim_set_hl(0, "TelescopeBorder", {fg=colors.pink, bg=nil})
vim.api.nvim_set_hl(0, "TelescopePromptBorder", {fg=colors.cyan, bg=nil})
vim.api.nvim_set_hl(0, "TelescopeResultsBorder", {fg=colors.pink, bg=nil})
vim.api.nvim_set_hl(0, "TelescopePreviewBorder", {fg=colors.purple, bg=nil})
