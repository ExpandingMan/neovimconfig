local Spectre = require("spectre")

WhichKey.add {
  { "\\S", Spectre.open, desc = "open spectre search and replace" },
  { "\\a", Spectre.open_file_search, desc = "open search and replace in current file only" },
}

WhichKey.add {
  { "\\fs", Spectre.open_visual, desc = "open spectre search and replace in visual mode", mode = "v" },
  { "\\fw", function() Spectre.open_visual({selected_word=true}) end, desc = "search and replace on selected word",
  mode = "v" },
}

Spectre.setup {
    color_devicons = true,
}

local Ssr = require("ssr")

Ssr.setup {
    keymaps = {
        close = "q",
        next_match = "n",
        prev_match = "N",
        replace_confirm = "<cr>",
        replace_all = "<leader><cr>",
    },
}

vim.keymap.set({"n", "x"}, "<leader>sr", Ssr.open)

WhichKey.add {
  { "\\r", Ssr.open, desc = "ssr tree-sitter search and replace" },
}
