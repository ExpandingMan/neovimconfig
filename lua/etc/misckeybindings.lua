
local function termopen(argfunc)
    return function()
        vim.notify("opening terminal", vim.log.levels.INFO, nil)
        --TODO: this is quite kitty specific now. wezterm doesn't have --detach or directory arg
        os.execute(vim.env.TERMINAL.." --detach "..argfunc().." &")
    end
end

-- directory stuff
WhichKey.add {
  { ",", group = "filesystem and directory functions" },
  { ",J", ":cd -<CR>:pwd<CR>", desc = "change to previous directory", remap = false },
  { ",a", ":set autochdir!", desc = "toggle auto directory switching", remap = false },
  { ",d", ":cd %:p:h<CR>:pwd<CR>", desc = "change to directory of current file", remap = false },
  { ",e", ":cd %:p:h/..<CR>:pwd<CR>", desc = "change to parent directory of current file", remap = false },
  { ",h", ":cd<CR>:pwd<CR>", desc = "change to home directory", remap = false },
  { ",p", ":pwd<CR>", desc = "show current working directory", remap = false },
  { ",t", termopen, desc = "open a new terminal in the current directory", remap = false },
}

-- REPL and terminal interaction stuff
WhichKey.add {
  { "<leader>r", group = "REPL and terminal interaction" },
  { "<leader>rl", ":Luapad<CR>", desc = "open a luapad", remap = false },
}


-- arrow bindings for switching windows
WhichKey.add {
  { "<down>", "<cmd>wincmd j<cr>", desc = "window, move down" },
  { "<left>", "<cmd>wincmd h<cr>", desc = "window, move left" },
  { "<right>", "<cmd>wincmd l<cr>", desc = "window, move right" },
  { "<up>", "<cmd>wincmd k<cr>", desc = "window, move up" },
}


-- easy scrolling consistent with my terminal setup
WhichKey.add {
  { "<C-j>", "<C-e>", desc = "scroll down one line", remap = false },
  { "<C-k>", "<C-y>", desc = "scroll up one line", remap = false },
}

-- package manager
WhichKey.add {
  { "<C-l>", "<cmd>Lazy<cr>", desc = "open lazy package manager", remap = false },
}


local function rifleopen(opts)
    return function()
        local word = vim.fn.expand("<cWORD>")
        vim.notify("rifle opening: "..word, vim.log.levels.INFO, nil)
        os.execute("rifle "..opts.." "..word.." &")
    end
end

WhichKey.add {
  { "gR", rifleopen, desc = "open object under cursor with rifle in new terminal" },
  { "gr", rifleopen, desc = "open object under cursor with rifle" },
}

