
Yazi = require("yazi")

Yazi.setup{
    open_for_directories=true,
    highlight_hovered_buffers_in_same_directory = true,
}

WhichKey.add{
    { ",", group="yazi" },
    { ",,", "<cmd>Yazi<cr>", desc="open yazi at current file", remap=false},
    { ",c", "<cmd>Yazi cwd<cr>", desc="open yazi in current directory", remap=false},
    { ",t", "<cmd>Yazi toggle<cr>", desc="open yazi at last location", remap=false},
}
