
local Surround = require("nvim-surround")
local Config = Surround.config


Surround.setup {
    -- disable default key maps
    keymaps = {
        insert = false,
        insert_line = false,
        normal = false,
        normal_cur = false,
        normal_line = false,
        normal_cur_line = false,
        visual = false,
        visual_line = false,
        delete = false,
        change = false,
    },
}


WhichKey.add {
  { "((", "<Plug>(nvim-surround-normal)", desc = "surround with delimiters" },
  { "()", "<Plug>(nvim-surround-normal-cur)", desc = "surround with delimiters on current line" },
  { "([", "<Plug>(nvim-surround-normal-line)", desc = "surround with delimiters on new lines" },
  { "(]", "<Plug>(nvim-surround-normal-cur-line)", desc = "surround lines with delimiters on new lines" },
  { "(c", "<Plug>(nvim-surround-change)", desc = "change surrounding delimiters" },
  { "(d", "<Plug>(nvim-surround-delete)", desc = "delete surrounding delimiters" },
}

WhichKey.add {
  { "<c-g>(", "<Plug>(nvim-surround-insert)", desc = "surround with delimiters", mode = "i" },
  { "<c-g>)", "<Plug>(nvim-surround-insert-line)", desc = "surround line with delimiters", mode = "i" },
}

WhichKey.add {
  { "((", "<Plug>(nvim-surround-visual)", desc = "surround with delimiters", mode = "v" },
  { "()", "<Plug>(nvim-surround-visual-line)", desc = "surround line with delimiters", mode = "v" },
}

