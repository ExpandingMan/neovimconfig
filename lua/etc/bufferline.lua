local Bufferline = require("bufferline")

local function print_numbers(opts)
    return string.format("%s", opts.raise(opts.ordinal), opts.raise((opts.id)))
end

Bufferline.setup({
    options = {
        numbers = print_numbers,
        close_command = "bdelete! %d",
        right_mouse_command = "bdelete! %d",
        left_mouse_command = "buffer %d",
        middle_mouse_command = nil,
        -- NOTE: this plugin is designed with this icon in mind,
        -- and so changing this is NOT recommended, this is intended
        -- as an escape hatch for people who cannot bear it for whatever reason
        indicator = {icon='◗'},
        buffer_close_icon = '',
        modified_icon = '●',
        close_icon = '',
        left_trunc_marker = '',
        right_trunc_marker = '',
        --- name_formatter can be used to change the buffer's label in the bufferline.
        --- Please note some names can/will break the
        --- bufferline so use this at your discretion knowing that it has
        --- some limitations that will *NOT* be fixed.
        name_formatter = function(buf)  -- buf contains a "name", "path" and "bufnr"
            -- TODO: would like to show letter or two from path but don't feel like fucking around
            -- with lua's shit stdlibs right now
            return buf.name
        end,
        max_name_length = 18,
        max_prefix_length = 15, -- prefix used when a buffer is de-duplicated
        tab_size = 18,
        diagnostics = false,  -- NOTE can do "nvim_lsp"
        diagnostics_indicator = function(count, level, diagnostics_dict, context)
            return "("..count..")"
        end,
        -- NOTE: this will be called a lot so don't do any heavy processing here
        -- TODO may want to set some of these
        custom_filter = function(buf_number)
            -- filter out filetypes you don't want to see
            if vim.bo[buf_number].filetype ~= "<i-dont-want-to-see-this>" then
                return true
            end
            -- filter out by buffer name
            if vim.fn.bufname(buf_number) ~= "<buffer-name-I-dont-want>" then
                return true
            end
            -- filter out based on arbitrary rules
            -- e.g. filter out vim wiki buffer from tabline in your work repo
            if vim.fn.getcwd() == "<work-repo>" and vim.bo[buf_number].filetype ~= "wiki" then
                return true
            end
        end,
        offsets = {{filetype = "NvimTree", text = "()", text_align = "left", highlight="String"}},
        show_buffer_icons = true, -- disable filetype icons for buffers
        show_buffer_close_icons = false,
        show_close_icon = false,
        show_tab_indicators = true,
        persist_buffer_sort = false, -- whether or not custom sorted buffers should persist
        -- can also be a table containing 2 custom separators
        -- [focused and unfocused]. eg: { '|', '|' }
        separator_style = {"▮", ""},
        enforce_regular_tabs = true,
        always_show_bufferline = false,
        sort_by = "id",
    },
    highlights = {
        fill = {
            bg = colors.bgdark,
            fg = colors.bgdark,
        },
        buffer_selected = {
            fg = colors.cyan,
        },
        separator = {
            fg = colors.purple,
        },
        pick = {
            fg = colors.purple,
        },
        pick_visible = {
            fg = colors.purple,
        },
        pick_selected = {
            fg = colors.green,
        },
        indicator_selected = {
            fg = colors.green,
        },
    }
})

local function temporal_compare(buf1, buf2)
    local t1 = vim.loop.fs_stat(buf1.path).mtime.sec
    local t2 = vim.loop.fs_stat(buf2.path).mtime.sec
    return t1 > t2
end

local function open_split_buf(n, vert)
    return function()
        require("bufferline.commands").exec(n, function(buf, all_visible)
            local str = vert and "vert sbuffer " or "sbuffer "
            vim.cmd(str..buf.id)
        end)
    end
end


WhichKey.add {
  { ";", group = "manage buffers and tabs" },
  { ";,", function() return Bufferline.close_in_direction("left") end, desc="close all buffers to the left" },
  { ";.", function() return Bufferline.close_in_direction("right") end, desc="close all buffers to the right" },
  { ";E", function() return Bufferline.sort_buffers_by("extension") end, desc="sort buffers by extension" },
  { ";S", function() return Bufferline.sort_buffers_by("directory") end, desc = "sort buffers by directory" },
  { ";T", function() return Bufferline.sort_buffers_by("tabs") end, desc = "sort buffers by tabs" },
  { ";[", function() return Bufferline.cycle(-1) end, desc = "cycle previous" },
  { ";]", function() return Bufferline.cycle(1) end, desc = "cycle next" },
  { ";d", function() return Bufferline.close_buffer_with_pick() end, desc = "pick buffer to close" },
  { ";p", function() return Bufferline.pick_buffer() end, desc = "pick a buffer" },
  { ";r", "<cmd>edit<cr>", desc = "refresh buffer from file; same as :e" },
  { ";x", function() return vim.cmd("bd") end, desc = "close current buffer" },
  { ";{", function() return Bufferline.move(-1) end, desc = "move to previous" },
  { ";}", function() return Bufferline.move(1) end, desc = "move to next" },
}

for j = 1, 10 do
    WhichKey.add {
        {";"..tostring(j % 10), function() return Bufferline.go_to_buffer(j, true) end,
        desc="go to buffer "..tostring(j)},
        {";s"..tostring(j % 10), open_split_buf(j, false),
        desc="open buffer "..tostring(j).." in new split"},
        {";v"..tostring(j % 10), open_split_buf(j, true),
        desc="open buffer "..tostring(j).." in new vertical split"},
    }
end

