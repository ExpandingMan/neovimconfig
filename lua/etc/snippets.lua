LuaSnip = require("luasnip")

local Events = require("luasnip.util.events")
local Loader = require("luasnip.loaders.from_vscode")

local parser = LuaSnip.parser

local s = LuaSnip.snippet
local sn = LuaSnip.snippet_node
local isn = LuaSnip.indent_snippet_node
local t = LuaSnip.text_node
local i = LuaSnip.insert_node
local f = LuaSnip.function_node
local c = LuaSnip.choice_node
local d = LuaSnip.dynamic_node

LuaSnip.config.set_config({
    history = true,
    enable_autosnippets = false,
    -- suggested use:
    -- highlight word with "bve", hit tab, type for snippets
    store_selection_keys = "<tab>",
})

-- see help LuaSnip
-- and example here: https://github.com/L3MON4D3/LuaSnip/blob/master/Examples/snippets.lua

LuaSnip.snippets = {
    all = {
    },
}

Loader.lazy_load({
    paths = {"~/.config/nvim/snippets"}
})


local _modes = {"i", "s", "n"}
for m = 1, #_modes do
    WhichKey.add {
      {
        mode = { _modes[m] },
        { "<c-s>", group = "snippet control" },
        { "<c-s>U", function() return LuaSnip.unlink_current_if_deleted() end, desc = "unlink current if deleted" },
        { "<c-s>a", ":LuaSnipListAvailable<CR>", desc = "list available snippets" },
        { "<c-s>h", function() return LuaSnip.change_choice(-1) end, desc = "previous snippet node choice" },
        { "<c-s>j", function() return LuaSnip.expand_or_jump() end,
        desc="expand snippet; or jump to next snippet position" },
        { "<c-s>k", function() return LuaSnip.jump(-1) end, desc = "jump to previous snippet position" },
        { "<c-s>l", function() return LuaSnip.change_choice(1) end, desc = "next snippet node choice" },
        { "<c-s>u", function() return LuaSnip.unlink_current() end, desc = "unlink current snippet" },
      },
    }
end
