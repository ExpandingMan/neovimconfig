local Aerial = require("aerial")

Aerial.setup({
    backends = {"treesitter", "markdown"},
    default_bindings = true,
    filter_kind = {
        "Class",
        "Constructor",
        "Enum",
        "Function",
        "Interface",
        "Method",
        "Struct",
    },
    highlight_mode = "split_width",
    highlight_on_jump = 300,  -- time in ms to highlight after jumping
    layout = {
        min_width = 16,
        default_direction = "prefer_right",
    },
})

WhichKey.add {
  { "<leader>a", group = "aerial code navigation" },
  { "<leader>aa", "<cmd>AerialToggle!<cr>", desc = "toggle aerial code navigation buffer" },
  { "<leader>an", "<cmd>AerialNext<cr>", desc = "jump forward one symbol" },
  { "<leader>ap", "<cmd>AerialPrev<cr>", desc = "jump backward one symbol" },
}
