local TSConfig = require("nvim-treesitter.configs")
local TSParsers = require("nvim-treesitter.parsers")

TSConfig.setup {
    ensure_installed = {"c", "lua", "julia", "rust", "zig", "fish", "nu"},
    auto_install = true,
    ignore_install = {"gitignore"},  -- requires node-js executable
    highlight = {
        disable = {"latex", "markdown"},  --these are better as non-tree-sitter
        enable = true,
    },
    additional_vim_regex_highlighting = true,
    indent = {enable=false},
}

local TSNodeAction = require("ts-node-action")
local Act = require("ts-node-action.actions")
local Help = require("ts-node-action.helpers")

local function julia_toggle_singleline(node)
    local o = {}

    local c = node:named_child(0)

    table.insert(o, Help.node_text(c) .. " = ")

    local txt = Help.node_text(c:next_named_sibling())
    if type(txt) == "string" then
        txt = {txt}
    end

    for i=1, #txt do
        if i == 1 then
            o[1] = o[1] .. txt[i]
        else
            table.insert(o, txt[i])
        end
    end
    
    return o
end

local function julia_toggle_multiline(node)
    local o = {}
    
    local c = node:named_child(0)

    --make sure function def and not some other assignment
    if c:type() ~= "call_expression" then
        return nil
    end

    local txt = Help.node_text(c)

    table.insert(o, "function "..txt)

    -- next is =, then is body
    c = c:next_named_sibling():next_named_sibling()

    table.insert(o, "    "..Help.node_text(c))
    table.insert(o, "end")

    return o
end

--TODO: sensible behavior when multiline
local function julia_ternary(node)
    local o = {}

    local c = node:named_child(0)

    table.insert(o, Help.node_text(c).." ? ")

    c = c:next_named_sibling()
    o[1] = o[1] .. Help.node_text(c) .. " : "

    c = c:next_named_sibling():named_child(0)
    o[1] = o[1] .. Help.node_text(c)

    return o
end

--TODO: this also needs a lot of cleanup
local function julia_ifelse(node)
    local o = {}
    
    local c = node:named_child(0)
    table.insert(o, "if "..Help.node_text(c))

    c = c:next_named_sibling()
    table.insert(o, "    "..Help.node_text(c))

    table.insert(o, "else")

    c = c:next_named_sibling()
    table.insert(o, "    "..Help.node_text(c))

    table.insert(o, "end")

    return o
end

TSNodeAction.setup {
    julia = {
        function_definition = {{julia_toggle_singleline, name = "to single line function"}},
        assignment = {{julia_toggle_multiline, name = "to multiline function"}},
        if_statement = {{julia_ternary, name = "to ternary conditional"}},
        ternary_expression = {{julia_ifelse, name = "to if else block"}},
    },
}

WhichKey.add {
  { "<leader>o", TSNodeAction.node_action, desc = "trigger TS node action" },
}
