local AutoSession = require("auto-session")

AutoSession.setup({
    log_level = "warn",
    auto_session_suppress_dirs = {"/", "~/", "~/Downloads"},
    auto_session_enable_last_session = false,
    auto_session_root_dir = vim.fn.stdpath("data").."/sessions/",
    auto_session_enabled = true,
    auto_save_enabled = false,
    auto_restore_enabled = false,
    auto_session_suppress_dirs = {},
    session_lens = {
        theme_conf = {border = true},
    },
})

WhichKey.add {
  { "<leader>s", group = "session management" },
  { "<leader>sL", "<cmd>RestoreSession", desc = "load session from specified directory path", remap = false },
  { "<leader>sS", "<cmd>SaveSession", desc = "save session in specified directory path", remap = false },
  { "<leader>sX", "<cmd>DeleteSession", desc = "delete session for specified directory path", remap = false },
  { "<leader>sl", "<cmd>RestoreSession<CR>", desc = "load session for current path", remap = false },
  { "<leader>ss", "<cmd>SaveSession<CR>", desc = "save the current session", remap = false },
  { "<leader>sx", "<cmd>DeleteSession<CR>", desc = "delete session for current path", remap = false },
}
