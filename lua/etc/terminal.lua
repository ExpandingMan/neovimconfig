local ToggleTerm = require("toggleterm")
local Terminal = require("toggleterm/terminal").Terminal
local Iron = require("iron.core")

local function termsize(term)
    if term.direction == "horizontal" then
        return 20
    elseif term.direction == "vertical" then
        return vim.o.columns * 0.3
    end
end

vim.api.nvim_set_hl(0, "ToggleTermBorder", {fg=colors.pink, bg=colorsbgdarker})
vim.api.nvim_set_hl(0, "ToggleTermBackground", {fg=colors.bgdark, bg=colors.bgdark})

ToggleTerm.setup({
    size = termsize,
    open_mapping = "<C-\\>",
    hide_numbers = false,
    shade_terminals = true,
    start_in_insert = true,
    insert_mappings = true,
    persist_size = true,
    direction = "horizontal",
    close_on_exit = true,
    shell = "nu",  -- vim.o.shell to get from env
    float_opts = {
        border = "curved",  -- can be "double", "shadow", "curved" or "single"
        --width = 80,
        --height = 60,
        winblend = 3,
        highlights = {
            border = "ToggleTermBorder",
            background = "ToggleTermBackground",
        }
    },
})

Iron.setup({
  config = {
    -- Whether a repl should be discarded or not
    scratch_repl = true,
    -- Your repl definitions come here
    repl_definition = {
      sh = {
        command = {"nu"},
      }
    },
    -- How the repl window will be displayed
    -- See below for more information
    repl_open_cmd = require('iron.view').split.vertical("50%")
  },
  -- If the highlight is on, you can change how it looks
  -- this is highlight for last sent block
  highlight = {
    italic = false,
  }
})

local function _iron_send_visual()
    Iron.mark_visual()
    Iron.send_mark()
end

WhichKey.add {
  { "<leader>r", group = "REPL and terminal interaction" },
  { "<leader>rL", "<cmd>Luapad<CR>", desc = "open a luapad" },
  { "<leader>rR", "<cmd>IronRestart<cr>", desc = "restart REPL" },
  { "<leader>rn", "<cmd>vs term://nu<CR>", desc = "open a nu terminal in a new vertical split" },
  { "<leader>rm", "<cmd>sp term://nu<CR>", desc = "open a nu terminal in a new horizontal split"},
  { "<leader>rf", "<cmd>vs term://fish<CR>", desc = "open a fish terminal in a new vertical split" },
  { "<leader>rh", "<cmd>lua _htop_term_toggle()<CR>", desc = "show htop in a floating terminal" },
  { "<leader>rj", "<cmd>lua _julia_term_toggle()<CR>", desc = "toggle a Julia terminal" },
  { "<leader>rl", _iron_send_visual, desc = "send line to REPL" },
  { "<leader>rr", "<cmd>IronRepl<cr>", desc = "open REPL" },
  { "<leader>rt", "<cmd>ToggleTerm<CR>", desc = "toggle terminal (can also do <C-\\>)" },
  { "<leader>rz", "<cmd>vs term://zsh<cr>", desc = "open a zsh terminal in a new vertical split" },

  { "<leader>r", _iron_send_visual, desc = "send selected block to REPL", mode="v" },
  { "<C-n>", "<cmd>stopinsert!<CR>", desc = "get out of insert mode", mode = "t", remap = false },
}

local htopterm = Terminal:new({
    cmd = "htop",
    direction = "float",
})
function _htop_term_toggle() return htopterm:toggle() end

local juliaterm = Terminal:new({
    cmd = "julia",
    direction = "horizontal",
})
function _julia_term_toggle() return juliaterm:toggle() end
