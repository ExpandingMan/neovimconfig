local Todo = require("todo-comments")

Todo.setup({
    signs = true,
    sign_priority = 8,
    keywords = {
        FIX = {
            --icon = " ", -- icon used for the sign, and in search results
            icon ="𝐄",
            color = "error", -- can be a hex color, or a named color (see below)
            alt = { "FUCK", "BUG", "FIXIT" }, -- a set of other keywords that all map to this FIX keywords
            -- signs = false, -- configure signs for some keywords individually
        },
        --TODO = { icon = " ", color = "hint" },
        TODO = {icon="𝐓", color="hint"},
        --HACK = { icon = " ", color = "warning" },
        HACK = {icon="𝐇", color="warning"},
        --WARN = { icon = " ", color = "warning", alt = { "WARNING", "TEMP" } },
        WARN = {icon="𝐖", color="warning", alt={"WARNING","TEMP"}},
        --PERF = { icon = "🧪", color = "test", alt = { "OPTIM", "PERFORMANCE", "OPTIMIZE" } },
        PERF = {icon="𝐏", color="test", alt={"OPTIM","PERFORMANCE","OPTIMIZE"}},
        --NOTE = { icon = "📔", color = "info", alt = { "INFO" } },
        NOTE = {icon="𝐍", color="info", alt={"INFO"}},
    },
    gui_style = {fg="NONE", bg="ITALIC"},
    merge_keyword = true,
    highlight = {
        before = "", -- "fg" or "bg" or empty
        keyword = "bg", -- "fg", "bg", "wide" or empty
        after = "", -- "fg" or "bg" or empty
        pattern = [[.*<(KEYWORDS)\s*:]], -- pattern used for highlightng (vim regex)
        comments_only = true,
        max_line_len = 400, -- ignore lines longer than this
        exclude = {}, -- list of file types to exclude highlighting
    },
    colors = { -- these often don't behave when linking to highlight groups
        error = { colors.red },
        warning = { colors.orange },
        info = { colors.selection },
        hint = { colors.yellow },
        default = { colors.foreground },
        test = { colors.cyan },
    },
    search = {
        command = "rg",
        args = {
            "--color=never",
            "--no-heading",
            "--with-filename",
            "--line-number",
            "--column",
        },
        -- regex that will be used to match keywords.
        -- don't replace the (KEYWORDS) placeholder
        pattern = [[\b(KEYWORDS):]], -- ripgrep regex
    }
})

WhichKey.add {
  { "<leader>y", group = "TODO management" },
  { "<leader>yf", "<cmd>TodoTelescope<CR>", desc = "telescope search for TODO and FIX notices", remap = false },
  { "\\t", "<cmd>TodoTelescope<cr>", desc = "telescope search for TODO and FIX notices", remap = false },
  { "<leader>yq", "<cmd>TodoQuickFix<CR>", desc = "bring up quick fix list", remap = false },
  { "<leader>yy", function() Todo.jump_next() end, desc = "jump to next TODO note" },
  { "<leader>yY", function() Todo.jump_prev() end, desc = "jump to previous TODO note" },
}
