vim.g.tex_flavor = "latex"
vim.g.vimtex_view_method = "zathura"
vim.g.vimtex_view_general_viewer = "zathura"
vim.g.vimtex_quickfix_mode = 0
vim.g.tex_conceal = "abdmg"
vim.g.vimtex_mappings_enabled = false  -- don't define default keybindings for vimtex
vim.g.typst_pdf_viewer = "zathura"  -- not latex

WhichKey.add {
  { "<leader>l", group = "LaTeX commands" },
  { "<leader>lC", "<cmd>VimtexCleanAll<CR>", desc = "clean all latex output", remap = false },
  { "<leader>lG", "<cmd>VimtexStatusAll<CR>", desc = "show latex compiler status for all", remap = false },
  { "<leader>lI", "<cmd>VimtexInfoFull<CR>", desc = "show full latex info", remap = false },
  { "<leader>lK", "<cmd>VimtexStopAll<CR>", desc = "stop all latex compilation", remap = false },
  { "<leader>lL", "<cmd>VimtexCompileSelected<CR>", desc = "start latex compilation for selection", remap = false },
  { "<leader>lT", "<cmd>VimtexTocToggle<CR>", desc = "toggle latex table of contents", remap = false },
  { "<leader>la", "<cmd>VimtexContextMenu<CR>", desc = "open latex context menu", remap = false },
  { "<leader>lc", "<cmd>VimtexClean<CR>", desc = "clean latex output", remap = false },
  { "<leader>le", "<cmd>VimtexErrors<CR>", desc = "show latex compilation errors", remap = false },
  { "<leader>lg", "<cmd>VimtexStatus<CR>", desc = "show latex compiler status", remap = false },
  { "<leader>li", "<cmd>VimtexInfo<CR>", desc = "show latex info", remap = false },
  { "<leader>lk", "<cmd>VimtexStop<CR>", desc = "stop latex compilation", remap = false },
  { "<leader>ll", "<cmd>VimtexCompile<CR>", desc = "start latex compilation", remap = false },
  { "<leader>lo", "<cmd>VimtexCompileOutput<CR>", desc = "open file where compiler is redirected", remap = false },
  { "<leader>lq", "<cmd>VimtexLog<CR>", desc = "show latex compiler log", remap = false },
  { "<leader>lt", "<cmd>VimtexTocOpen<CR>", desc = "open latex table of contents", remap = false },
  { "<leader>lu", ":call LaTeXtoUnicode#Toggle()<CR>", desc = "toggle latex-to-unicode", remap = false },
  { "<leader>lv", "<cmd>VimtexView<CR>", desc = "view latex output", remap = false },
  { "<leader>lx", "<cmd>VimtexReload<CR>", desc = "reload vimtex", remap = false },
  { "<leader>ly", "<cmd>TypstWatch<CR>", desc = "watch typst file", remap = false },
}

-- tex is another of those things that has very unique syntax, so we define highlights here
vim.api.nvim_set_hl(0, "texBeginEndName", {fg=colors.orange, italic=true})
vim.api.nvim_set_hl(0, "texBoldItalStyle", {fg=colors.orange, bold=true, italic=true})
vim.api.nvim_set_hl(0, "texBoldStyle", {fg=colors.orange, bold=true})
vim.api.nvim_set_hl(0, "texInputFile", {fg=colors.orange, italic=true})
vim.api.nvim_set_hl(0, "texStyleItal", {fg=colors.yellow, italic=true})
vim.api.nvim_set_hl(0, "texLigature", {fg=colors.purple})
vim.api.nvim_set_hl(0, "texMath", {fg=colors.red})
vim.api.nvim_set_hl(0, "texMathMatcher", {fg=colors.red})
vim.api.nvim_set_hl(0, "texMathSymbol", {fg=colors.cyan})
vim.api.nvim_set_hl(0, "texSpecialChar", {fg=colors.purple})
vim.api.nvim_set_hl(0, "texSubscripts", {fg=colors.pink})
vim.api.nvim_set_hl(0, "texTitle", {fg=colors.foreground, bold=true})

